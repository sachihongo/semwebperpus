import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ListbookComponent} from './listbook/listbook.component';
import {DetailbookComponent} from './detailbook/detailbook.component';
import {TestComponent} from './test/test.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'listbook', component: ListbookComponent },
    { path: 'detailbook', component: DetailbookComponent },
    { path: 'testbook', component: TestComponent }
    ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
