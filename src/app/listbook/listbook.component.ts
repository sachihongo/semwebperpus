import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from  '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-listbook',
  templateUrl: './listbook.component.html',
  styleUrls: ['./listbook.component.scss']
})

export class ListbookComponent implements OnInit {
  public firstname: string;

  // results: any = {};
  books: any;
  constructor(private router: Router, private httpClient: HttpClient, private route: ActivatedRoute) {

    this.route.queryParams.subscribe(params => {
      this.firstname = params['firstname'];
      console.log(this.firstname);
    });

    this.httpClient.post('http://localhost:9000/api/search', {
      name: this.firstname,
    } )
    // .map((res: Response) => res.json())
    // .pipe(map(res => res.data))
    //     .pipe(map(res => res.json()))
        .subscribe(
            data  => {
              this.books = data;
              console.log('POST Request is successful', data);
              console.log(this.books);
            },
            error  => {

              console.log('Error', error);
            });
  }

  ngOnInit() {

  }

  goToDetail() {
    this.router.navigate(['/testbook']);
  }

  goTo() {
      this.router.navigate(['/testbook']);
  }

}
