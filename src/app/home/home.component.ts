import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private keyname;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToList() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        firstname: this.keyname
      }
    };
    // console.log(navigationExtras);
    this.router.navigate(['/listbook'], navigationExtras );
  }
}
